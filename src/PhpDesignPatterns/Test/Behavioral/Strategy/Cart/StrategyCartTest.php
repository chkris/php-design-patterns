<?php

namespace PhpDesignPatterns\Test\Behavioral\Strategy\Cart;

use PHPUnit_Framework_TestCase;

use PhpDesignPatterns\Behavioral\Strategy\Cart\Domain\Customer;
use PhpDesignPatterns\Behavioral\Strategy\Cart\Domain\Cart;
use PhpDesignPatterns\Behavioral\Strategy\Cart\Item\Car\Audi;
use PhpDesignPatterns\Behavioral\Strategy\Cart\Item\Car\Fiat;
use PhpDesignPatterns\Behavioral\Strategy\Cart\Item\Car\Opel;
use PhpDesignPatterns\Behavioral\Strategy\Cart\Item\Car\Ford;
use PhpDesignPatterns\Behavioral\Strategy\Cart\Domain\Payment\PayPalPayment;
use PhpDesignPatterns\Behavioral\Strategy\Cart\Domain\Payment\MobilePayment;
use PhpDesignPatterns\Behavioral\Strategy\Cart\Domain\Payment\CreditCardPayment;
use PhpDesignPatterns\Behavioral\Strategy\Cart\ObjectValue\Money;

class StrategyCartTest extends PHPUnit_Framework_TestCase
{
    public function testPaymentStrategy()
    {
        $customer = new Customer;
        $cart = new Cart;

        $products = array(new Audi, new Fiat, new Ford, new Opel);

        $cart->acceptProducts($products);
        $order = $cart->prepareOrder();

        $this->assertEquals('Order number: 12/08/FV. The Customer paid 11500 EUR via PayPal.',
            $customer->pay($order,
                new PayPalPayment(
                    new Money($order->getTotalPrice())
                )
            )
        );

        $this->assertEquals('Order number: 12/08/FV. The Customer paid 11500 EUR via Mobile.',
            $customer->pay($order,
                new MobilePayment(
                    new Money($order->getTotalPrice())
                )
            )
        );

        $this->assertEquals('Order number: 12/08/FV. The Customer paid 11500 EUR via CreditCard.',
            $customer->pay($order,
                new CreditCardPayment(
                    new Money($order->getTotalPrice())
                )
            )
        );
    }
}
