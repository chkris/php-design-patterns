<?php

namespace PhpDesignPatterns\Test\Behavioral\Strategy\Calculator;

use PhpDesignPatterns\Behavioral\Strategy\Calculator\Calculator;

use PhpDesignPatterns\Behavioral\Strategy\Calculator\ArithmeticMean;
use PhpDesignPatterns\Behavioral\Strategy\Calculator\GeometricMean;
use PhpDesignPatterns\Behavioral\Strategy\Calculator\HarmonicMean;

use PHPUnit_Framework_TestCase;

class StrategyCalculatorTest extends PHPUnit_Framework_TestCase
{
    private function getInput()
    {
        return array(1, 5, 8, 12, 23, 78);
    }

    public function testArithmeticMean()
    {
        $calculator = new Calculator();
        $calculator ->setMeanCalculationStrategy(new ArithmeticMean);

        $this->assertEquals(21.17, $calculator->calculateMean($this->getInput()), '', 0.01);
    }

    public function testGeometricMean()
    {
        $calculator = new Calculator();
        $calculator ->setMeanCalculationStrategy(new GeometricMean());

        $this->assertEquals(9.75, $calculator->calculateMean($this->getInput()), '', 0.01);
    }

    public function testHarmonicMean()
    {
        $calculator = new Calculator();
        $calculator ->setMeanCalculationStrategy(new HarmonicMean());

        $this->assertEquals(4.09, $calculator->calculateMean($this->getInput()), '', 0.01);
    }
}
