<?php

namespace PhpDesignPatterns\Test\Behavioral\Strategy\Calculator;

use PhpDesignPatterns\Behavioral\State\Season\SeasonContext;

use PHPUnit_Framework_TestCase;

class StateSeasonTest extends PHPUnit_Framework_TestCase
{
    public function testArithmeticMean()
    {
        $seasonContext = new SeasonContext;

        $this->assertEquals('Winter' ,$seasonContext->getCurrentSeason()->getDescription());
        $this->assertEquals('solid' ,$seasonContext->getCurrentSeason()->getWaterState());

        $seasonContext->next();
        $this->assertEquals('Spring' ,$seasonContext->getCurrentSeason()->getDescription());
        $this->assertEquals('liquid' ,$seasonContext->getCurrentSeason()->getWaterState());

        $seasonContext->next();
        $this->assertEquals('Summer' ,$seasonContext->getCurrentSeason()->getDescription());
        $this->assertEquals('liquid' ,$seasonContext->getCurrentSeason()->getWaterState());

        $seasonContext->next();
        $this->assertEquals('Autumn' ,$seasonContext->getCurrentSeason()->getDescription());
        $this->assertEquals('liquid' ,$seasonContext->getCurrentSeason()->getWaterState());
    }
}
