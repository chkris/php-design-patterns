<?php

namespace PhpDesignPatterns\Test\Behavioral\TemplateMethod\Tax;

use PhpDesignPatterns\Behavioral\TemplateMethod\Tax\PolishTaxProvider;
use PhpDesignPatterns\Behavioral\TemplateMethod\Tax\UnitedKingdomTaxProvider;
use PHPUnit_Framework_TestCase;

class TemplateMethodTest extends PHPUnit_Framework_TestCase
{
    public function testAdapteeAndTarget()
    {
        $cash = 200000;

        $taxProvider = new PolishTaxProvider();

        $this->assertEquals('In Poland you must pay you tax in next week otherwise US will get all what you have got', $taxProvider->manageTaxes($cash));

        $taxProvider = new UnitedKingdomTaxProvider();

        $this->assertEquals('You do not need pay tax until you create one more place for work', $taxProvider->manageTaxes($cash));
    }

}
