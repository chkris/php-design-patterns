<?php

namespace PhpDesignPatterns\Test\Behavioral\TemplateMethod\Pizza;

use PhpDesignPatterns\Behavioral\TemplateMethod\Pizza\VegetarianPizza;
use PhpDesignPatterns\Behavioral\TemplateMethod\Pizza\MeetPizza;
use PHPUnit_Framework_TestCase;

class TemplateMethodTest extends PHPUnit_Framework_TestCase
{
    public function testAdapteeAndTarget()
    {
        $vegetarianPizza = new VegetarianPizza();
        $meetPizza = new MeetPizza();

        $this->assertEquals('Pizza with: mozzarella, beef, poultry, pork, bacon', $meetPizza->createPizza());
        $this->assertEquals('Pizza with: onion, tomato, garlic, pepper, marrow, mozzarella', $vegetarianPizza->createPizza());
    }

}
