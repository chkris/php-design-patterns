<?php

namespace PhpDesignPatterns\Test\Behavioral\Observer\Stock;

use PHPUnit_Framework_TestCase;

use PhpDesignPatterns\Behavioral\Observer\Stock\StockGrabber;
use PhpDesignPatterns\Behavioral\Observer\Stock\StockObserver;

class ObserverStockTest extends PHPUnit_Framework_TestCase
{
    public function testStockNotification()
    {
        $stockGrabber = new StockGrabber;

        $stockObserverOne = new StockObserver($stockGrabber);

        $this->assertEquals(0 , $stockObserverOne->getMicrosoftPrice(), 0.01);
        $this->assertEquals(0 , $stockObserverOne->getGooglePrice(), 0.01);

        $stockGrabber->setMicrosoftPrice(234.51);
        $stockGrabber->setGooglePrice(145.42);

        $this->assertEquals(234.51 , $stockObserverOne->getMicrosoftPrice(), 0.01);
        $this->assertEquals(145.42 , $stockObserverOne->getGooglePrice(), 0.01);

        //$stockObserverTwo is before $stockGrabber action it have initial values
        $stockObserverTwo = new StockObserver($stockGrabber);

        $this->assertEquals(0 , $stockObserverTwo->getMicrosoftPrice(), 0.01);
        $this->assertEquals(0 , $stockObserverTwo->getGooglePrice(), 0.01);

        //$stockObserverOne and $stockObserverTwo have the same information from $stockGrabber
        $stockGrabber->setMicrosoftPrice(236.51);
        $stockGrabber->setGooglePrice(149.42);

        $this->assertEquals(236.51 , $stockObserverOne->getMicrosoftPrice(), 0.01);
        $this->assertEquals(149.42 , $stockObserverOne->getGooglePrice(), 0.01);
        $this->assertEquals(236.51 , $stockObserverTwo->getMicrosoftPrice(), 0.01);
        $this->assertEquals(149.42 , $stockObserverTwo->getGooglePrice(), 0.01);

        //$stockObserverOne will be no longer notified about changes in stock market
        $stockGrabber->unRegister($stockObserverOne);

        $stockGrabber->setMicrosoftPrice(333.51);
        $stockGrabber->setGooglePrice(333.42);

        $this->assertEquals(236.51 , $stockObserverOne->getMicrosoftPrice(), 0.01);
        $this->assertEquals(149.42 , $stockObserverOne->getGooglePrice(), 0.01);
        $this->assertEquals(333.51 , $stockObserverTwo->getMicrosoftPrice(), 0.01);
        $this->assertEquals(333.42 , $stockObserverTwo->getGooglePrice(), 0.01);
    }
}
