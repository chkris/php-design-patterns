<?php

namespace PhpDesignPatterns\Test\Creational\Factory\Writer;

use PHPUnit_Framework_TestCase;

use PhpDesignPatterns\Creational\Factory\Writer\WriterFactory;

class FactoryWriterTest extends PHPUnit_Framework_TestCase
{
    public function testWrtiers()
    {
        $writerFactory = new WriterFactory();
        $data = array('Microsoft', 'Google');

        $writer = $writerFactory->make('xml');
        $expected = '
<?xml version="1.0" encoding="UTF-8"?>
<stock>
    <company>Microsoft</company>
    <company>Google</company>
</stock>
';
        $this->assertEquals($expected, $writer->write($data));

        $writer = $writerFactory->make('json');
        $expected = '
{
    "stock" : {
        "company" : [
            {"value" : "Microsoft" },
            {"value" : "Google" }
        ]
    }
 }
';
        $this->assertEquals($expected, $writer->write($data));
    }
}
