<?php

namespace PhpDesignPatterns\Test\Creational\AbstractFactory\LookAndFeel;

use PHPUnit_Framework_TestCase;

use PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Factory\WindowsFactory;
use PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Factory\OsxFactory;

class AbstractFactoryLookAndFeelTest extends PHPUnit_Framework_TestCase
{
    public function testLookAndFeel()
    {
        $factory = new WindowsFactory;

        $this->assertEquals('Windows Button Title', $factory->createButton()->title());
        $this->assertEquals('Windows Window Size', $factory->createWindow()->size());

        $factory = new OsxFactory;

        $this->assertEquals('OSX Button Title', $factory->createButton()->title());
        $this->assertEquals('OSX Window Size', $factory->createWindow()->size());
    }
}
