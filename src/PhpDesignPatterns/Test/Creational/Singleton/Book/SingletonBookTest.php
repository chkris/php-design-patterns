<?php

namespace PhpDesignPatterns\Test\Creational\Singleton\Book;

use PHPUnit_Framework_TestCase;

use PhpDesignPatterns\Creational\Singleton\Book\PrototypeLibrary;
use PhpDesignPatterns\Creational\Singleton\Book\BookBorrower;

class SingletonBookTest extends PHPUnit_Framework_TestCase
{
    public function testBookBorrow()
    {
        $library = new PrototypeLibrary();
        $bookBorrower = new BookBorrower();

        $bookBorrower->borrowBook($library->getBook('The Lord of the Rings'));

        $this->assertEquals('Tolkien', $bookBorrower->getBorrowedBook()->getAuthor());
        $this->assertEquals(1137, $bookBorrower->getBorrowedBook()->getNumberOfPages());
        $this->assertEquals('hard', $bookBorrower->getBorrowedBook()->getCoverType());

        $this->assertEquals('The book was borrowed from Prototype National library', $library->isBookBorrowed('The Lord of the Rings'));

        $library->acceptBook($bookBorrower->returnBook());

        $this->assertEquals('The book is in Prototype National Library', $library->isBookBorrowed('The Lord of the Rings'));
    }
}
