<?php

namespace PhpDesignPatterns\Test\Structural\Decorator\Pizza;

use PhpDesignPatterns\Structural\Decorator\Pizza\Ingredients\Cheese;
use PhpDesignPatterns\Structural\Decorator\Pizza\Ingredients\TomatoSauce;
use PhpDesignPatterns\Structural\Decorator\Pizza\PlainPizza;

class DecoratorTest extends \PHPUnit_Framework_TestCase
{
    public function testCreateOnlyDough()
    {
        $plainPizza = new PlainPizza();
        $this->assertEquals(5.00, $plainPizza->getCost());
        $this->assertEquals('pizza made of: dough', $plainPizza->getDescription());
    }

    public function testDecoratePizzaWithCheeseAndTomatoSauce()
    {
        $plainPizza = new TomatoSauce(new Cheese(new PlainPizza()));
        $this->assertEquals(8.00, $plainPizza->getCost());
        $this->assertEquals('pizza made of: dough, cheese, tomato sauce', $plainPizza->getDescription());
    }
}
