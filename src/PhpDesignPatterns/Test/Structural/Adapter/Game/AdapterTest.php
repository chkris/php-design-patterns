<?php

namespace PhpDesignPatterns\Test\Structural\Adapter\Game;

use PHPUnit_Framework_TestCase;

use PhpDesignPatterns\Structural\Adapter\Game\AttackerRobotAdapter;
use PhpDesignPatterns\Structural\Adapter\Game\Robot;
use PhpDesignPatterns\Structural\Adapter\Game\Tank;

class AdapterTest extends PHPUnit_Framework_TestCase
{
    public function testAdapteeAndTarget()
    {
        $tank = new Tank();
        $robot = new Robot();

        $damage = 10;

        $this->assertEquals('Tank does '.$damage.' damage', $tank->fireWeapon($damage));
        $this->assertEquals('Robot does '.$damage.' damage', $robot->smashWithHands($damage));
    }

    public function testAdapter()
    {
        $robot = new Robot();
        $attackerRobotAdapter = new AttackerRobotAdapter($robot);

        $damage = 10;

        $this->assertEquals('Robot does '.$damage.' damage' , $attackerRobotAdapter->fireWeapon($damage));
    }
}
