<?php

namespace PhpDesignPatterns\Test\Structural\Adapter\Game;

use PhpDesignPatterns\Structural\Adapter\WebService\Client;
use PhpDesignPatterns\Structural\Adapter\WebService\Services\Facebook;
use PhpDesignPatterns\Structural\Adapter\WebService\Services\Google;
use PhpDesignPatterns\Structural\Adapter\WebService\ServicesAdapters\FacebookAdapter;
use PhpDesignPatterns\Structural\Adapter\WebService\ServicesAdapters\GoogleAdapter;

class AdapterWebServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testServices()
    {
        $facebook = new Facebook();
        $google = new Google('google');

        $facebookAdapter = new FacebookAdapter($facebook);
        $googleAdapter = new GoogleAdapter($google);

        $client = new Client();

        $client
            ->addService($facebookAdapter)
            ->addService($googleAdapter);

        $client->sendMessages('message');

        $answers = $client->getServicesAnswers();

        $this->assertEquals('Facebook post: message' , $answers[0]);
        $this->assertEquals('Posted on Google+. Desc: message' , $answers[1]);
    }
}
