<?php

namespace PhpDesignPatterns\Creational\Singleton\Book;

use PhpDesignPatterns\Creational\Singleton\Book\TheLordOfTheRingsBook;

use Exception;
use InvalidArgumentException;

/**
 * This is special case of Library storing original, prototype of all books written by their authors.
 * It means that there is only one instance of book
 */
class PrototypeLibrary
{
    const THE_LORD_OF_THE_RINGS = 'The Lord of the Rings';

    // If not borrowed yet status is true
    protected $books = array(
        self::THE_LORD_OF_THE_RINGS => true,
    );

    /**
     * @param string $title
     */
    public function getBook($title)
    {
        $book = null;

        if (true == $this->books[$title]) {
            $this->books[$title] = false;
            $book = $this->bringBook($title);
        }

        return $book;
    }

    /**
     *
     * @param  string                   $title
     * @return string
     * @throws InvalidArgumentException
     */
    public function isBookBorrowed($title)
    {
        try {
            if (false == $this->books[$title]) {
                return 'The book was borrowed from Prototype National library';
            } else {
                return 'The book is in Prototype National Library';
            }
        } catch (Exception $e) {
            throw new InvalidArgumentException('Such book doesn\'t exist');
        }
    }

    public function acceptBook(Book $book)
    {
        $this->books[$book->getTitle()] = true;

        $book->returnBook();
    }

    protected function bringBook($title)
    {
        switch ($title) {
            case self::THE_LORD_OF_THE_RINGS: return TheLordOfTheRingsBook::borrowBook();
        }
    }
}
