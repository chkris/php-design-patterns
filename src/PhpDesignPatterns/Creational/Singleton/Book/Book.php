<?php

namespace PhpDesignPatterns\Creational\Singleton\Book;

interface Book
{
    public function getTitle();
    public function getAuthor();
    public function getNumberOfPages();
    public function getCoverType();
    public static function borrowBook();
    public function returnBook();
}
