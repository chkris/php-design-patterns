<?php

namespace PhpDesignPatterns\Creational\Singleton\Book;

use PhpDesignPatterns\Creational\Singleton\Book\Book;

/**
 * This borower can have only one book
 */
class BookBorrower
{
    /**
     * @var \PhpDesignPatterns\Creational\Singleton\Book\Book $book
     */
    protected $book;

    /**
     * @var \PhpDesignPatterns\Creational\Singleton\Book\PrototypeLibrary $library
     */
    protected $library;

    /**
     * @param \PhpDesignPatterns\Creational\Singleton\Book\Book $book
     */
    public function borrowBook(Book $book)
    {
        $this->book = $book;
    }

    /**
     * @return \PhpDesignPatterns\Creational\Singleton\Book\Book
     */
    public function getBorrowedBook()
    {
        return $this->book;
    }

    public function returnBook()
    {
        return $this->book;
    }

    protected function setLibrary(PrototypeLibrary $library)
    {
        $this->library = $library;
    }
}
