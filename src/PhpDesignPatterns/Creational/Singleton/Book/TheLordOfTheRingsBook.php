<?php

namespace PhpDesignPatterns\Creational\Singleton\Book;

use PhpDesignPatterns\Creational\Singleton\Book\Book;

class TheLordOfTheRingsBook implements Book
{
    private static $instance;

    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}

    public static function borrowBook()
    {
        if (null == self::$instance) {
            self::$instance = new TheLordOfTheRingsBook;
        }

        return self::$instance;
    }

    public function returnBook()
    {
       self::$instance = null;
    }

    public function getTitle()
    {
        return 'The Lord of the Rings';
    }

    public function getAuthor()
    {
        return 'Tolkien';
    }

    public function getCoverType()
    {
        return 'hard';
    }

    public function getNumberOfPages()
    {
        return 1137;
    }
}
