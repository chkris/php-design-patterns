<?php

namespace PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Product;

use PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Product\Button;

class OsxButton implements Button
{
    public function title()
    {
        return 'OSX Button Title';
    }
}
