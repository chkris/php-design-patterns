<?php

namespace PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Product;

use PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Product\Window;

class WinWindow implements Window
{
    public function size()
    {
         return 'Windows Window Size';
    }
}
