<?php

namespace PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Product;

use PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Product\Window;

class OsxWindow implements Window
{
    public function size()
    {
        return 'OSX Window Size';
    }
}
