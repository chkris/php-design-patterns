<?php

namespace PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Product;

interface Button
{
    public function title();
}
