<?php

namespace PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Product;

use PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Product\Button;

class WinButton implements Button
{
    public function title()
    {
        return 'Windows Button Title';
    }
}
