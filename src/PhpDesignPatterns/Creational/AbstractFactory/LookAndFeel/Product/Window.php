<?php

namespace PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Product;

interface Window
{
    public function size();
}
