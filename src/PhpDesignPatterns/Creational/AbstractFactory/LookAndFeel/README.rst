
========================
Abstract Factory Pattern
========================

Intent
======

* Interface for creating families of relates objects without specifying their concrete class

UML - WIKI
==========

.. image:: http://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Abstract_factory_UML.svg/1000px-Abstract_factory_UML.svg.png