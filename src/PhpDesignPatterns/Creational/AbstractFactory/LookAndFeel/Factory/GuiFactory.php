<?php

namespace PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Factory;

interface GuiFactory
{
    public function createButton();
    public function createWindow();
}
