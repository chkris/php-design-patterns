<?php

namespace PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Factory;

use PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Factory\GuiFactory;

use PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Product\OsxButton;
use PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Product\OsxWindow;

class OsxFactory  implements GuiFactory
{
    public function createButton()
    {
        return new OsxButton;
    }

    public function createWindow()
    {
        return new OsxWindow;
    }
}
