<?php

namespace PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Factory;

use PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Factory\GuiFactory;

use PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Product\WinButton;
use PhpDesignPatterns\Creational\AbstractFactory\LookAndFeel\Product\WinWindow;

class WindowsFactory implements GuiFactory
{
    public function createButton()
    {
        return new WinButton;
    }

    public function createWindow()
    {
        return new WinWindow;
    }
}
