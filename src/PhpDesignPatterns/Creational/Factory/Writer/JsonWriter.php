<?php

namespace PhpDesignPatterns\Creational\Factory\Writer;

use PhpDesignPatterns\Creational\Factory\Writer\Writer;

class JsonWriter implements Writer
{
    public function close()
    {
    }

    public function open()
    {
    }

    /**
     * format here using for example formater service and write
     */
    public function write($data)
    {
        return '
{
    "stock" : {
        "company" : [
            {"value" : "'. $data[0] .'" },
            {"value" : "'. $data[1] .'" }
        ]
    }
 }
';
    }
}
