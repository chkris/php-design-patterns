<?php

namespace PhpDesignPatterns\Creational\Factory\Writer;

use PhpDesignPatterns\Creational\Factory\Writer\Writer;

class XmlWriter implements Writer
{
    public function close()
    {
    }

    public function open()
    {
    }

    /**
     * format here using for example formater service and write
     */
    public function write($data)
    {
        return '
<?xml version="1.0" encoding="UTF-8"?>
<stock>
    <company>'. $data[0] .'</company>
    <company>'. $data[1] .'</company>
</stock>
';
    }

}
