<?php

namespace PhpDesignPatterns\Creational\Factory\Writer;

/**
 * open, close, write define common interface for creating classes with similar functionality
 */
interface Writer
{
    public function open();
    public function close();
    public function write($data);
}
