<?php

namespace PhpDesignPatterns\Creational\Factory\Writer;

use PhpDesignPatterns\Creational\Factory\Writer\JsonWriter;
use PhpDesignPatterns\Creational\Factory\Writer\XmlWriter;

class WriterFactory
{
    public function make($key)
    {
        switch ($key) {
            case 'json':
                return new JsonWriter();
                break;
            case 'xml':
                return new XmlWriter();
                break;
        }

        return null;
    }
}
