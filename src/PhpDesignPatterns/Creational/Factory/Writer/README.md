Factory Pattern
=================

Intent:
    * Encapsulate creation of similar objects
    * Refers to a newly created object through common interface

Uml elements:
    Factory - creates concrete product
    Product - interface for creating similar objects