<?php

namespace PhpDesignPatterns\Structural\Decorator\Pizza;

use PhpDesignPatterns\Structural\Decorator\Pizza\Pizza;

class PlainPizza implements Pizza
{
    public function getDescription()
    {
        return 'pizza made of: dough';
    }

    public function getCost()
    {
        return 5.00;
    }
}
