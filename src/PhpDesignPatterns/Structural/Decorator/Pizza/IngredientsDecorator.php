<?php

namespace PhpDesignPatterns\Structural\Decorator\Pizza;

use PhpDesignPatterns\Structural\Decorator\Pizza\Pizza;

abstract class IngredientsDecorator implements Pizza
{
    protected $pizza;

    /**
     * @param Pizza $pizza
     */
    public function __construct(Pizza $pizza)
    {
        $this->pizza = $pizza;
    }

    public function getDescription()
    {

    }

    public function getCost()
    {

    }
}
