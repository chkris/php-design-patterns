<?php

namespace PhpDesignPatterns\Structural\Decorator\Pizza\Ingredients;

use PhpDesignPatterns\Structural\Decorator\Pizza\IngredientsDecorator;
use PhpDesignPatterns\Structural\Decorator\Pizza\Pizza;

class Cheese extends IngredientsDecorator
{
    /**
     * @param Pizza $pizza
     */
    public function __construct(Pizza $pizza)
    {
        parent::__construct($pizza);
    }

    public function getDescription()
    {
        return $this->pizza->getDescription().', cheese';
    }

    public function getCost()
    {
        return $this->pizza->getCost() + 0.50;
    }
}
