<?php

namespace PhpDesignPatterns\Structural\Decorator\Pizza;

interface Pizza
{
    public function getDescription();
    public function getCost();
}
