Decorator Pattern
=================

Intent:
    * Attach additional functionality(extend or modify the behavior) to object dynamically
    * Not affecting the behavior of the other object from the same class
    * Wraps original class with new behavior
    * Allow to create composition of difficult(containing many elements) classes instead of statically extends new behavior,
      example CarWithFourWheelThreeDoorsAndAirConditioning can be compose(assembly) from 4 x Wheel, 3 x Doors, 1 x AirConditioning.
      If Car in the future should have also GpsNavigation than we don't need to extend   CarWithFourWheelThreeDoorsAndAirConditioningGpsNavigation
      but just implement new class GpsNavigation and decorate the Car.

Uml elements:
    Component - Interface for object that can have responsibilities added to them dynamically
    ConcreteComponent - Defines object to witch additional responsibilities can be added
    Decorator - Abstract class holding reference to Component and define interface
    ConcreteDecorator - extends functionality of Component adding or hiding behaviors.