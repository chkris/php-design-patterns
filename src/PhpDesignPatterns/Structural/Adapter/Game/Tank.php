<?php

namespace PhpDesignPatterns\Structural\Adapter\Game;

use PhpDesignPatterns\Structural\Adapter\Game\Attacker;

/**
 * Class Tank implementation of Target interface
 * @package PhpDesignPatterns\Adapter\Game
 */
class Tank implements Attacker
{
    public function fireWeapon($damage)
    {
        return 'Tank does '.$damage.' damage';
    }

    public function moveForward($miles)
    {
        return 'Tank moves '.$miles.' miles';
    }

    public function assignDriver($driver)
    {
        return $driver.' can drive the Tank';
    }
}
