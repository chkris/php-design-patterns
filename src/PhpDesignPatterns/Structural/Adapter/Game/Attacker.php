<?php

namespace PhpDesignPatterns\Structural\Adapter\Game;

/**
 * Class Attacker = Target
 * @package PhpDesignPatterns\Structural\Adapter\Game
 */
interface Attacker
{
    public function fireWeapon($damage);
    public function moveForward($miles);
    public function assignDriver($driver);
}
