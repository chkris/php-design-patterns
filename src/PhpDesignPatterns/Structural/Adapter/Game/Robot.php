<?php

namespace PhpDesignPatterns\Structural\Adapter\Game;

/**
 * Class Robot = Adaptee
 * @package PhpDesignPatterns\Structural\Adapter\Game
 */
class Robot
{
    public function smashWithHands($damage)
    {
        return 'Robot does '.$damage.' damage';
    }

    public function walkForward($miles)
    {
        return 'Robot walk '.$miles;
    }

    public function reactToHuman($human)
    {
        return 'React to human '.$human;
    }
}
