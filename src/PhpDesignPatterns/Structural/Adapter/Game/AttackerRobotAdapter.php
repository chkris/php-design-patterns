<?php

namespace PhpDesignPatterns\Structural\Adapter\Game;

use PhpDesignPatterns\Structural\Adapter\Game\Attacker;
use PhpDesignPatterns\Structural\Adapter\Game\Robot;

/**
 * Class AttackerRobotAdapter = Adapter
 * @package PhpDesignPatterns\Structural\Adapter\Game
 */
class AttackerRobotAdapter implements Attacker
{
    /**
     * @param PhpDesignPatterns\Adapter\Game\Robot $robot
     */
    protected $robot;

    public function __construct(Robot $robot)
    {
        $this->robot = $robot;
    }

    public function fireWeapon($damage)
    {
        return $this->robot->smashWithHands($damage);
    }

    public function moveForward($miles)
    {
        return $this->robot->walkForward($miles);
    }

    public function assignDriver($driver)
    {
        return $this->robot->reactToHuman($driver);
    }
}
