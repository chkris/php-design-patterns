<?php

namespace PhpDesignPatterns\Structural\Adapter\WebService\Services;

/**
 * Class Facebook
 * @package PhpDesignPatterns\Structural\Adapter\WebService\Services
 */
class Facebook
{
    protected $post;

    public function authenticate()
    {
        return 'Authenticated with Facebook';
    }

    public function post($post)
    {
        $this->post = $post;

        return 'Facebook post: '.$post;
    }

    public function getPost()
    {
        return $this->post;
    }
}
