<?php

namespace PhpDesignPatterns\Structural\Adapter\WebService\Services;

/**
 * Class Twitter
 * @package PhpDesignPatterns\Structural\Adapter\WebService\Services
 */
class Twitter
{
    protected $name;
    protected $key;
    protected $tweet;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function twitterAuthentication($key)
    {
        $this->key = $key;
    }

    public function sendTweet($tweet)
    {
        $this->tweet = $tweet;

        return 'Tweet posted on Tweeter. Desc: '.$tweet;
    }
}
