<?php

namespace PhpDesignPatterns\Structural\Adapter\WebService\Services;

/**
 * Class Google
 * @package PhpDesignPatterns\Structural\Adapter\WebService\Services
 */
class Google
{
    protected $name;
    protected $post;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function googleAuthentication()
    {
    }

    public function sendPost($post)
    {
        $this->post = $post;

        return 'Posted on Google+. Desc: '.$post;
    }

    public function getPost()
    {
        return $this->post;
    }
}
