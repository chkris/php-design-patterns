<?php

namespace PhpDesignPatterns\Structural\Adapter\WebService;

/**
 * Class ServiceInterface - adapter interface to provide unified communication protocol for Client class for Services like  Twitter or Facebook
 * @package PhpDesignPatterns\Structural\Adapter\WebService
 */
interface ServiceInterface
{
    public function authorize($options);
    public function post($post);
}
