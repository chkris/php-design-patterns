<?php

namespace PhpDesignPatterns\Structural\Adapter\WebService;

class Client
{
    /**
     * @var ServiceInterface $service
     */
    protected $services = array();

    /**
     * @var array
     */
    protected $serviceAnswers = array();

    /**
     * @param  ServiceInterface $service
     * @return Client
     */
    public function addService(ServiceInterface $service)
    {
        $this->services[] = $service;

        return $this;
    }

    public function sendMessages($message)
    {
        foreach ($this->services as $service) {
            $this->addServiceAnswer($service->post($message));
        }
    }

    public function addServiceAnswer($answer)
    {
        $this->serviceAnswers[] = $answer;
    }

    public function getServicesAnswers()
    {
        return $this->serviceAnswers;
    }
}
