<?php

namespace PhpDesignPatterns\Structural\Adapter\WebService\ServicesAdapters;

use PhpDesignPatterns\Structural\Adapter\WebService\ServiceInterface;
use PhpDesignPatterns\Structural\Adapter\WebService\Services\Google;

class GoogleAdapter implements ServiceInterface
{
    protected $google;

    public function __construct(Google $google)
    {
        $this->google = $google;
    }

    public function authorize($options)
    {
        $this->google->googleAuthentication();
    }

    public function post($post)
    {
        return $this->google->sendPost($post);
    }
}
