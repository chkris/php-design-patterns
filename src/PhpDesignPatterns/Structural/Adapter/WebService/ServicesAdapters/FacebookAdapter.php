<?php

namespace PhpDesignPatterns\Structural\Adapter\WebService\ServicesAdapters;

use PhpDesignPatterns\Structural\Adapter\WebService\ServiceInterface;
use PhpDesignPatterns\Structural\Adapter\WebService\Services\Facebook;

class FacebookAdapter implements ServiceInterface
{
    protected $facebook;

    public function __construct(Facebook $facebook)
    {
        $this->facebook = $facebook;
    }

    public function authorize($options)
    {
        $this->facebook->authenticate();
    }

    public function post($post)
    {
        return $this->facebook->post($post);
    }
}
