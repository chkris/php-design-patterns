Adapter Pattern
=================

Intent:
    * Converts interface into another client expect
    * Lets classes work together that couldn't otherwise because of incompatible interfaces

Uml elements:
    Client - Require to use of incompatible type. It expect to interact with Target interface but the class it gong to use is incompatible Adaptee.
    Target - Client expect this interface to interact with.
    Adapter - Provide link between incompatible Client and Adaptee.
    Adaptee - This class contains functionality required by Client but the interface is incompatible and the Client cannot communicate directly with Adaptee.