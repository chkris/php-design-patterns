<?php

namespace PhpDesignPatterns\Behavioral\Strategy\Calculator;

class HarmonicMean implements Mean
{
    public function calculateMean(array $input)
    {
        $sum = 0;
        array_walk($input, function($key) use (&$input, &$sum) {
            $sum += 1/$key;
        });

        return count($input)/$sum;
    }
}
