<?php

namespace PhpDesignPatterns\Behavioral\Strategy\Calculator;

class GeometricMean implements Mean
{
    public function calculateMean(array $input)
    {
        return pow(array_product($input), 1/count($input));
    }
}
