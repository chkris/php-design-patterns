<?php

namespace PhpDesignPatterns\Behavioral\Strategy\Calculator;

class ArithmeticMean implements Mean
{
    public function calculateMean(array $input)
    {
        return array_sum($input)/count($input);
    }
}
