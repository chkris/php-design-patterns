<?php

namespace PhpDesignPatterns\Behavioral\Strategy\Calculator;

class Calculator
{
    protected $meanCalculationStrategy;

    public function setMeanCalculationStrategy(Mean $meanCalculationStrategy)
    {
        $this->meanCalculationStrategy = $meanCalculationStrategy;
    }

    public function calculateMean(array $input)
    {
        return $this->meanCalculationStrategy->calculateMean($input);
    }
}
