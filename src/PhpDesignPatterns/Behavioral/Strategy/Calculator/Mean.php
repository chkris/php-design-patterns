<?php

namespace PhpDesignPatterns\Behavioral\Strategy\Calculator;

/**
 * Class Mean common interface for creating various kind of algorithms
 * @package PhpDesignPatterns\Behavioral\Strategy\Calculator
 */
interface Mean
{
    public function calculateMean(array $input);
}
