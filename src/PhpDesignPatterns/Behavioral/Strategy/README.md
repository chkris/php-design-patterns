Strategy Pattern
=================

Intent:
    * Allow one family of algorithms to be selected on-the-fly at runtime

Uml elements:
    Context - Contains reference to strategy object.
    Strategy - Common interface to all supported algorithms.
    ConcreteStrategy - each class implement variant algorithm with a Strategy interface