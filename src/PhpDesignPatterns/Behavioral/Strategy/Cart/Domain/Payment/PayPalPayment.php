<?php

namespace PhpDesignPatterns\Behavioral\Strategy\Cart\Domain\Payment;

use PhpDesignPatterns\Behavioral\Strategy\Cart\ObjectValue\Money;

class PayPalPayment implements Payment
{
    public function __construct(Money $money)
    {
    }

    public function pay($amountOfCash)
    {
        //connect to account with email and password and make payment
    }

    public function getPaymentName()
    {
        return 'PayPal';
    }
}
