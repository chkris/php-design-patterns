<?php

namespace PhpDesignPatterns\Behavioral\Strategy\Cart\Domain\Payment;

interface Payment
{
    public function getPaymentName();
    public function pay($amountOfCash);
}
