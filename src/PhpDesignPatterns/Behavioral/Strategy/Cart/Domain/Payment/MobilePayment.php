<?php

namespace PhpDesignPatterns\Behavioral\Strategy\Cart\Domain\Payment;

use PhpDesignPatterns\Behavioral\Strategy\Cart\Domain\Payment\Payment;

class MobilePayment implements Payment
{
    public function getPaymentName()
    {
        return 'Mobile';
    }

    public function pay($amountOfCash)
    {
        //use phone to pay cash
    }
}
