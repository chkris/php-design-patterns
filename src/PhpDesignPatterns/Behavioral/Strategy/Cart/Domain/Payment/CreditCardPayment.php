<?php

namespace PhpDesignPatterns\Behavioral\Strategy\Cart\Domain\Payment;

class CreditCardPayment implements Payment
{
    public function getPaymentName()
    {
        return 'CreditCard';
    }

    public function pay($amountOfCash)
    {
        //provide pin and pay
    }
}
