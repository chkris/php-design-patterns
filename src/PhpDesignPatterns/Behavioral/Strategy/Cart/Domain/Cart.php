<?php

namespace PhpDesignPatterns\Behavioral\Strategy\Cart\Domain;

use PhpDesignPatterns\Behavioral\Strategy\Cart\Domain\Order;

class Cart
{
    protected $products;

    /**
     *
     * @param Item|Array $products
     */
    public function acceptProducts(array $products)
    {
        $this->products = $products;
    }

    /**
     *
     * @return \PhpDesignPatterns\Behavioral\Strategy\Cart\Domain\Order
     */
    public function prepareOrder()
    {
        $order = new Order();

        foreach ($this->products as $product) {
            $order->addProduct($product);
        }

        return $order;
    }
}
