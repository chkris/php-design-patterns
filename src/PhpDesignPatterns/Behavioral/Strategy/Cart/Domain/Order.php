<?php

namespace PhpDesignPatterns\Behavioral\Strategy\Cart\Domain;

use PhpDesignPatterns\Behavioral\Strategy\Cart\Item\Item;

class Order
{
    protected $productList;
    protected $number;

    public function __construct()
    {
        /*
         * Provide here next order number for example using OrderNumberProvider.
         * Set fake number
         */
        $this->number = '12/08/FV';
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function addProduct(Item $product)
    {
        $this->productList[] = $product;
    }

    public function getTotalPrice()
    {
        $totalPrice = 0;
        foreach ($this->productList as $product) {
            $totalPrice += $product->getPrice();
        }

        return $totalPrice;
    }

}
