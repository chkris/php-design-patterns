<?php

namespace PhpDesignPatterns\Behavioral\Strategy\Cart\Domain;

use PhpDesignPatterns\Behavioral\Strategy\Cart\Domain\Payment\Payment;
use PhpDesignPatterns\Behavioral\Strategy\Cart\Domain\Order;

class Customer
{
    /**
     *
     * @param \PhpDesignPatterns\Behavioral\Strategy\Cart\Domain\Order           $order
     * @param \PhpDesignPatterns\Behavioral\Strategy\Cart\Domain\Payment\Payment $payment
     */
    public function pay(Order $order, Payment $payment)
    {
        $payment->pay($order->getTotalPrice());

        return \sprintf('Order number: %s. The Customer paid %s EUR via %s.',
            $order->getNumber(),
            $order->getTotalPrice(),
            $payment->getPaymentName()
        );
    }
}
