<?php

namespace PhpDesignPatterns\Behavioral\Strategy\Cart\Item;

class Item
{
    protected $price;
    protected $name;

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getName()
    {
        return $this->name;
    }
}
