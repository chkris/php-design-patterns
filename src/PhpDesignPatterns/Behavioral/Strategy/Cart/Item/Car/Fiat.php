<?php

namespace PhpDesignPatterns\Behavioral\Strategy\Cart\Item\Car;

use \PhpDesignPatterns\Behavioral\Strategy\Cart\Item\Item;

class Fiat extends Item
{
    public function __construct()
    {
        $this->setName('Fiat');
        $this->setPrice(2000);
    }
}
