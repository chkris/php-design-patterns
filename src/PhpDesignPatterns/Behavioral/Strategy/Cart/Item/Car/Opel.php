<?php

namespace PhpDesignPatterns\Behavioral\Strategy\Cart\Item\Car;

use \PhpDesignPatterns\Behavioral\Strategy\Cart\Item\Item;

class Opel extends Item
{
    public function __construct()
    {
        $this->setName('Opel');
        $this->setPrice(1500);
    }
}
