<?php

namespace PhpDesignPatterns\Behavioral\Strategy\Cart\Item\Car;

use \PhpDesignPatterns\Behavioral\Strategy\Cart\Item\Item;

class Audi extends Item
{
    public function __construct()
    {
        $this->setName('Audi');
        $this->setPrice(5000);
    }
}
