<?php

namespace PhpDesignPatterns\Behavioral\Strategy\Cart\Item\Car;

use \PhpDesignPatterns\Behavioral\Strategy\Cart\Item\Item;

class Ford extends Item
{
    public function __construct()
    {
        $this->setName('Ford');
        $this->setPrice(3000);
    }
}
