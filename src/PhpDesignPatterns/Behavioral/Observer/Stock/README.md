Observer Pattern
=================

Intent:
    * Defines one to many relations between objects so that when one change state all its dependencies are notified and updated.

Uml elements:
    Observable - interface defining operations of attaching, de-attaching observers and notify the observers. 
    Observer - interface defining operation to be used to notify object implementing this interface.
    ConcreteObservabale - implements Observable interface
    ConscreteObserver - implements Observer interface