<?php

namespace PhpDesignPatterns\Behavioral\Observer\Stock;

class StockObserver implements Observer
{
    /**
     *
     * @var PhpDesignPatterns\Behavioral\Observer\Stock\StockGrabber $stockGrabber
     */
    protected $stockGrabber;

    protected $microsoftPrice;
    protected $googlePrice;

    /**
     *
     * @param \PhpDesignPatterns\Behavioral\Observer\Stock\StockGrabber $stockGrabber
     */
    public function __construct(StockGrabber $stockGrabber)
    {
        $this->stockGrabber = $stockGrabber;
        $this->stockGrabber->register($this);
        $this->microsoftPrice = 0;
        $this->googlePrice = 0;
    }

    /**
     *
     * @param double $microsoftPrice
     * @param double $googlePrice
     */
    public function update($microsoftPrice, $googlePrice)
    {
        $this->microsoftPrice = $microsoftPrice;
        $this->googlePrice = $googlePrice;
    }

    public function getMicrosoftPrice()
    {
        return $this->microsoftPrice;
    }

    public function getGooglePrice()
    {
        return $this->googlePrice;
    }
}
