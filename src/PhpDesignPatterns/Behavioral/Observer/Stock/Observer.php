<?php

namespace PhpDesignPatterns\Behavioral\Observer\Stock;

interface Observer
{
    public function update($microsoftPrice, $googlePrice);
}
