<?php

namespace PhpDesignPatterns\Behavioral\Observer\Stock;

interface Observable
{
    public function register(Observer $observer);
    public function unRegister(Observer $observer);
    public function notifyObserver();
}
