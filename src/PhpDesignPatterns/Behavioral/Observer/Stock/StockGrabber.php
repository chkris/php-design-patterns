<?php

namespace PhpDesignPatterns\Behavioral\Observer\Stock;

class StockGrabber implements Observable
{
    protected $observers;
    protected $microsoftPrice;
    protected $googlePrice;

    public function setMicrosoftPrice($microsoftPrice)
    {
        $this->microsoftPrice = $microsoftPrice;

        $this->notifyObserver();
    }

    public function setGooglePrice($googlePrice)
    {
        $this->googlePrice = $googlePrice;

        $this->notifyObserver();
    }

    public function notifyObserver()
    {
        foreach ($this->observers as $observer) {
            $observer->update($this->microsoftPrice, $this->googlePrice);
        }
    }

    public function register(Observer $observer)
    {
        $this->observers[] = $observer;
    }

    public function unRegister(Observer $observer)
    {
        $key = array_search($observer, $this->observers);

        if (null !== $key) {
            unset($this->observers[$key]);
        }
    }
}
