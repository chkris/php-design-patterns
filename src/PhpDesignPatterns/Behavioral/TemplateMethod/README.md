Template Method Pattern
=================

Intent:
    * Having many steps in algorithm and they can be implemented in different ways
    * Used to create a group of subclasses that have to execute similar group of methods

Uml elements:
    AbstractClass - have abstract methods and templateMethod()
    ConcreteClass - implements AbstractClass