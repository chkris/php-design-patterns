<?php

namespace PhpDesignPatterns\Behavioral\TemplateMethod\Pizza;

abstract class Pizza
{
    protected $pizza = 'Pizza with: ';

    final public function createPizza()
    {

        if ($this->withVegetables()) {
            $this->pizza .= $this->addVegetables();
        }

        if ($this->withCheese()) {
            $this->pizza .= $this->addCheese();
        }

        if ($this->withMeet()) {
            $this->pizza .= $this->addMeet();
        }

        return $this->pizza;
    }

    public function withVegetables()
    {
        return true;
    }

    public function withCheese()
    {
        return true;
    }

    public function withMeet()
    {
        return true;
    }

    /**
     * Those function must be implemented by subclasses
     */
    abstract public function addVegetables();
    abstract public function addCheese();
    abstract public function addMeet();
}
