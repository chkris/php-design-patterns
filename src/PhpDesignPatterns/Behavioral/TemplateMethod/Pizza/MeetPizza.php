<?php

namespace PhpDesignPatterns\Behavioral\TemplateMethod\Pizza;

use PhpDesignPatterns\Behavioral\TemplateMethod\Pizza\Pizza;

class MeetPizza extends Pizza
{
    protected $meetList = array('beef', 'poultry', 'pork', 'bacon');
    protected $cheese = 'mozzarella';

    public function withVegetables()
    {
        return false;
    }

    public function addVegetables() {}

    public function addCheese()
    {
        return $this->cheese.', ';
    }

    public function addMeet()
    {
        return implode(', ', $this->meetList);
    }
}
