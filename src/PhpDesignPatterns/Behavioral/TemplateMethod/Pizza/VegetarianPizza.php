<?php

namespace PhpDesignPatterns\Behavioral\TemplateMethod\Pizza;

use PhpDesignPatterns\Behavioral\TemplateMethod\Pizza\Pizza;

class VegetarianPizza extends Pizza
{
    protected $vegetables = array('onion', 'tomato', 'garlic', 'pepper', 'marrow');
    protected $cheese = 'mozzarella';

    public function withMeet()
    {
        return false;
    }

    public function addVegetables()
    {
        return implode(', ', $this->vegetables);
    }

    public function addCheese()
    {
        return ', '.$this->cheese;
    }

    public function addMeet() {}
}
