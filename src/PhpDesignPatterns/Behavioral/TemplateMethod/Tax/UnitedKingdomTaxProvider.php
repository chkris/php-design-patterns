<?php

namespace PhpDesignPatterns\Behavioral\TemplateMethod\Tax;

use PhpDesignPatterns\Behavioral\TemplateMethod\Tax\TaxProvider;

class UnitedKingdomTaxProvider extends  TaxProvider
{
    /**
     * Here some really complicated computations
     */
    public function calculateTax($cash)
    {
        return 0;
    }

    public function determineTaxLevelNotification($tax)
    {
        return ($tax > 12) ?  'You don not pay any tax' : 'You do not need pay tax until you create one more place for work';
    }
}
