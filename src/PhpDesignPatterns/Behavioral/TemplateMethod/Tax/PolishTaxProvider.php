<?php

namespace PhpDesignPatterns\Behavioral\TemplateMethod\Tax;

use PhpDesignPatterns\Behavioral\TemplateMethod\Tax\TaxProvider;

class PolishTaxProvider extends  TaxProvider
{
    /**
     * levels are just an examples not real values
     */
    protected $levels = array(
        '10000' => '11',
        '40000' => '19',
        '80000' => '32'
    );

    protected $minimalTaxLevel = 10000;

    public function calculateTax($cash)
    {
        foreach ($this->levels as $levelKey => $levelValue) {
            if ($levelKey > $cash) {
                return $cash * $levelValue;
            }
        }
    }

    public function determineTaxLevelNotification($tax)
    {
        return ($tax < $this->minimalTaxLevel) ?  'In Poland you must pay you tax in next week otherwise US will get all what you have got' : 'Don not worry you have all year to pay your taxes';
    }
}
