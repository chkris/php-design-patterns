<?php

namespace PhpDesignPatterns\Behavioral\TemplateMethod\Tax;

abstract class TaxProvider
{
    public function manageTaxes($cash)
    {
        $tax = $this->calculateTax($cash);

        return $this->determineTaxLevelNotification($tax);
    }

    /**
     * Calculate tax with respect to country and special requirements
     * @param  double $cash
     * @return tax
     */
    abstract public function calculateTax($cash);

    /**
     * Determine if amount of tax level provision. For example in Poland when cash > 80 000 then have to pay 30%
     * @param  double $tax
     * @return string notification
     */
    abstract public function determineTaxLevelNotification($tax);
}
