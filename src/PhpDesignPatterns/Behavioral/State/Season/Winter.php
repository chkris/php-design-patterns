<?php

namespace PhpDesignPatterns\Behavioral\State\Season;

use PhpDesignPatterns\Behavioral\State\Season\Spring;
use PhpDesignPatterns\Behavioral\State\Season\SeasonContext;

class Winter implements SeasonState
{
    public function getDescription()
    {
        return 'Winter';
    }

    public function next(SeasonContext $context)
    {
        $context->setCurrentState(new Spring);

        return $context->getCurrentSeason();
    }

    public function getWaterState()
    {
        return 'solid';
    }
}
