<?php

namespace PhpDesignPatterns\Behavioral\State\Season;

use PhpDesignPatterns\Behavioral\State\Season\Winter;
use PhpDesignPatterns\Behavioral\State\Season\SeasonState;

class SeasonContext
{
    /**
     * @var \PhpDesignPattern\Behavioral\State\Season\Winter $currentState
     */
    protected $currentState;

    public function __construct()
    {
        $this->currentState = new Winter;
    }

    public function next()
    {
        $this->currentState->next($this);
    }

    public function getCurrentSeason()
    {
        return $this->currentState;
    }

    public function setCurrentState(SeasonState $currentState)
    {
        $this->currentState = $currentState;
    }
}
