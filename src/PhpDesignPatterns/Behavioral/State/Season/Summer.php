<?php

namespace PhpDesignPatterns\Behavioral\State\Season;

use PhpDesignPatterns\Behavioral\State\Season\Autumn;
use PhpDesignPatterns\Behavioral\State\Season\SeasonContext;

class Summer implements SeasonState
{
    public function getDescription()
    {
        return 'Summer';
    }

    public function next(SeasonContext $context)
    {
        $context->setCurrentState(new Autumn);

        return $context->getCurrentSeason();
    }

    public function getWaterState()
    {
        return 'liquid';
    }
}
