<?php

namespace PhpDesignPatterns\Behavioral\State\Season;

use PhpDesignPatterns\Behavioral\State\Season\SeasonContext;

interface SeasonState
{
    public function getDescription();
    public function getWaterState();
    public function next(SeasonContext $context);
}
