<?php

namespace PhpDesignPatterns\Behavioral\State\Season;

use PhpDesignPatterns\Behavioral\State\Season\Summer;
use PhpDesignPatterns\Behavioral\State\Season\SeasonContext;

class Spring implements SeasonState
{

    public function getDescription()
    {
        return 'Spring';
    }

    public function next(SeasonContext $context)
    {
        $context->setCurrentState(new Summer);

        return $context->getCurrentSeason();
    }

    public function getWaterState()
    {
        return 'liquid';
    }
}
