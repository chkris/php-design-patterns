<?php

namespace PhpDesignPatterns\Behavioral\State\Season;

use PhpDesignPatterns\Behavioral\State\Season\Winter;
use PhpDesignPatterns\Behavioral\State\Season\SeasonContext;

class Autumn implements SeasonState
{
    public function getDescription()
    {
        return 'Autumn';
    }

    public function next(SeasonContext $context)
    {
        $context->setCurrentState(new Winter);

        return $context->getCurrentSeason();
    }

    public function getWaterState()
    {
        return 'liquid';
    }
}
