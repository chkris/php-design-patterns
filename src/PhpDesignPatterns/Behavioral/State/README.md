=============
State Pattern
=============

Intent: 
    * Allow an object to alter its behavior when its internal state change.The object will appear to change its class.

Uml elements:
    Context - Defines an interface of interest to client. Maintain an instance of a ConcreteState that defines the current state.
    State - Defines an interface for encapsulating the behavior associated with a particular state of a Context.
    ConcreteState - Each subclass implements a behavior associated with a state of the Context. 