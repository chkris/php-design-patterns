
========
Overview
========

Testing project with design patterns implemented in php

How to run
==========

* clone git repository
* composer install,
* run tests: ./vendor/bin/phpunit

Patterns
========

Behavioral:

* Observer                  - example: Stock
* Strategy                  - examples: Cart, Calculator
* Template method           - examples: Pizza, Tax 
* State                     - examples: Season
* Mediator                  - examples: Stock, Chat (java-design-patterns)
 
* Chain of responsibility
* Command
* Interpreter
* Iterator
* Memento


Structural:

* Adapter                   - examples: Game, WebService 
* Decorator                 - example: Pizza
* Composite                 - example: Item (java-design-pattern)

* Bridge
* Facade
* Flyweight
* Proxy


Creational:

* Factory                   - example: Writer
* Abstract factory          - example: LookAndFeel
* Singleton                 - example: Book

* Builder
* Prototype